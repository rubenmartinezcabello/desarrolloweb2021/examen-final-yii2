<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CiudadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ciudades');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciudad-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ciudad'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'habitantes',
            /*[
                'attribute'=>'escudo',
                'format' => ['image',['width'=>'auto','height'=>'100']],
                'value'=> function($model){ return $model->escudo; },
            ],*/
            [
                //'attribute' => 'escudo',
                'header' => Yii::t('app','Escudo'),
                'label' => Yii::t('app','Escudo'),
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::img($data['escudo'],
                    ['width' => '100px']);
                },
            ],
            [ //
                //'attribute' => 'mapa',
                'header' => Yii::t('app','Mapa'),
                'label' => Yii::t('app','Mapa'),
                'format' => 'raw',
                'value' => function($model){
                    return empty($model['mapa'])? '' : '<iframe src="https://www.google.com/maps/embed?pb=' . $model['mapa'] .
                        '" width="200" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>'; 
                    },
            ],
            [
                'header' => Yii::t('app', 'Actions'),
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>


</div>
