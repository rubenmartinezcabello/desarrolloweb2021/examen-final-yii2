<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Ciudades' => 'Ciudades',
    'Create Ciudad' => 'Agregar una ciudad',
    'Delete' => 'Eliminar',
    'Escudo' => 'Escudo',
    'Habitantes' => 'Nº de Habitantes',
    'ID' => 'Id.',
    'Mapa' => 'Google Maps',
    'Nombre' => 'Nombre',
    'Reset' => 'Restaurar',
    'Save' => 'Guardar',
    'Search' => 'Buscar',
    'The requested page does not exist.' => 'La página que busca no existe',
    'Update' => 'Actualizar',
    'Update Ciudad: {name}' => 'Actualiza la ciudad: {name}',
    'Actions' => 'Acciones',
    'Are you sure you want to delete this item?' => '¿Esta seguro de querer borrar este elemento?',
];
