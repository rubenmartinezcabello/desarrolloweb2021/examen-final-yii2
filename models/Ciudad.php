<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciudades".
 *
 * @property int $id
 * @property string|null $nombre
 * @property float|null $habitantes
 * @property string|null $escudo
 * @property string|null $mapa
 */
class Ciudad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciudades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['habitantes'], 'number'],
            [['nombre', 'escudo', 'mapa'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'habitantes' => Yii::t('app', 'Habitantes'),
            'escudo' => Yii::t('app', 'Escudo'),
            'mapa' => Yii::t('app', 'Mapa'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return CiudadQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CiudadQuery(get_called_class());
    }
}
